#include <SPI.h>
#include <LoRa.h>
#include "ds3231.h"
#include <WiFi.h>
#include <SD.h>

OLED_CLASS_OBJ display(OLED_ADDRESS, OLED_SDA, OLED_SCL);

#define WIFI_SSID       "hip"
#define WIFI_PASSWORD   "!0801hoya"
const unsigned long UpdateInterval = 2.5 * (60L * 1000000L); // Update delay in microseconds, currently 15-secs (1/4 of minute)
#define BUILTIN_LED 2
void setup()
{
    Serial.begin(115200);
    while (!Serial);

    if (OLED_RST > 0) {
        pinMode(OLED_RST, OUTPUT);
        digitalWrite(OLED_RST, HIGH);
        delay(100);
        digitalWrite(OLED_RST, LOW);
        delay(100);
        digitalWrite(OLED_RST, HIGH);
    }

//    display.init();
//    display.flipScreenVertically();
//    display.clear();
//    display.setFont(ArialMT_Plain_16);
//    display.setTextAlignment(TEXT_ALIGN_CENTER);
//    display.drawString(display.getWidth() / 2, display.getHeight() / 2, LORA_SENDER ? "LoRa Sender" : "LoRa Receiver");
//    display.display();
//    delay(2000);
//
//    if (SDCARD_CS >  0) {
//        display.clear();
//        SPIClass sdSPI(VSPI);
//        sdSPI.begin(SDCARD_SCLK, SDCARD_MISO, SDCARD_MOSI, SDCARD_CS);
//        if (!SD.begin(SDCARD_CS, sdSPI)) {
//            display.drawString(display.getWidth() / 2, display.getHeight() / 2, "SDCard  FAIL");
//        } else {
//            display.drawString(display.getWidth() / 2, display.getHeight() / 2 - 16, "SDCard  PASS");
//            uint32_t cardSize = SD.cardSize() / (1024 * 1024);
//            display.drawString(display.getWidth() / 2, display.getHeight() / 2, "Size: " + String(cardSize) + "MB");
//        }
//        display.display();
//        delay(2000);
//    }
//
//
//    String info = ds3231_test();
//    if (info != "") {
//        display.clear();
//        display.setFont(ArialMT_Plain_16);
//        display.setTextAlignment(TEXT_ALIGN_CENTER);
//        display.drawString(display.getWidth() / 2, display.getHeight() / 2, info);
//        display.display();
//        delay(2000);
//    }

    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
//        display.clear();
        Serial.println("WiFi Connect Fail");
//        display.drawString(display.getWidth() / 2, display.getHeight() / 2, "WiFi Connect Fail");
//        display.display();
        delay(2000);
        esp_restart();
    }
    Serial.print("Connected : ");
    Serial.println(WiFi.SSID());
    Serial.print("IP:");
    Serial.println(WiFi.localIP().toString());
//    display.clear();
//    display.drawString(display.getWidth() / 2, display.getHeight() / 2, "IP:" + WiFi.localIP().toString());
//    display.display();
    delay(2000);

    SPI.begin(CONFIG_CLK, CONFIG_MISO, CONFIG_MOSI, CONFIG_NSS);
    LoRa.setPins(CONFIG_NSS, CONFIG_RST, CONFIG_DIO0);
    if (!LoRa.begin(BAND)) {
        Serial.println("Starting LoRa failed!");
        while (1);
    }
//    if (!LORA_SENDER) {
//        display.clear();
//        display.drawString(display.getWidth() / 2, display.getHeight() / 2, "LoraRecv Ready");
//        display.display();
//    }
}

int count = 0;
void start_sleep(){
  esp_sleep_enable_timer_wakeup(UpdateInterval);
  pinMode(BUILTIN_LED,INPUT);     // Set pin-5 to an input as sometimes PIN-5 is used for SPI-SS
  digitalWrite(BUILTIN_LED,HIGH); // In case it's on, turn LED off, as sometimes PIN-5 on some boards is used for SPI-SS and can be left low
  Serial.println("Starting deep-sleep period... awake for "+String(millis())+"mS");
  delay(8); // Enough time for the serial port to finish at 115,200 baud
  esp_deep_sleep_start();         // Sleep for the prescribed time
}
void loop()
{
#if LORA_SENDER
    int32_t rssi;
    if (WiFi.status() == WL_CONNECTED) {
        rssi = WiFi.RSSI();
//        display.clear();
//        display.setTextAlignment(TEXT_ALIGN_CENTER);
//        display.drawString(display.getWidth() / 2, display.getHeight() / 2, "Send RSSI:" + String(rssi));
//        display.display();
        LoRa.beginPacket();
        LoRa.print("WiFi RSSI: ");
        LoRa.print(rssi);
        LoRa.endPacket();
        LoRa.sleep();
    } else {
        Serial.println("WiFi Connect lost ...");
    }
    delay(2500);
    start_sleep();
#else
    if (LoRa.parsePacket()) {
        String recv = "";
        while (LoRa.available()) {
            recv += (char)LoRa.read();
        }
        count++;
//        display.clear();
//        display.drawString(display.getWidth() / 2, display.getHeight() / 2, recv);
        String info = "[" + String(count) + "]" + "RSSI " + String(LoRa.packetRssi());
//        display.drawString(display.getWidth() / 2, display.getHeight() / 2 - 16, info);
//        display.display();
    }
#endif
}
