/*
 ESP8266 --> ThingSpeak Channel
 
 This sketch sends the Wi-Fi Signal Strength (RSSI) of an ESP8266 to a ThingSpeak
 channel using the ThingSpeak API (https://www.mathworks.com/help/thingspeak).
 
 Requirements:
 
   * ESP8266 Wi-Fi Device
   * Arduino 1.8.8+ IDE
   * Additional Boards URL: http://arduino.esp8266.com/stable/package_esp8266com_index.json
   * Library: esp8266 by ESP8266 Community
   * Library: ThingSpeak by MathWorks
 
 ThingSpeak Setup:
 
   * Sign Up for New User Account - https://thingspeak.com/users/sign_up
   * Create a new Channel by selecting Channels, My Channels, and then New Channel
   * Enable one field
   * Enter SECRET_CH_ID in "secrets.h"
   * Enter SECRET_WRITE_APIKEY in "secrets.h"
 Setup Wi-Fi:
  * Enter SECRET_SSID in "secrets.h"
  * Enter SECRET_PASS in "secrets.h"
  
 Tutorial: http://nothans.com/measure-wi-fi-signal-levels-with-the-esp8266-and-thingspeak
   
 Created: Feb 1, 2017 by Hans Scharler (http://nothans.com)
*/

#include "secrets.h"

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

#include <ESP8266WiFi.h>

char ssid[] = SECRET_SSID;   // your network SSID (name)
char pass[] = SECRET_PASS;   // your network password
int keyIndex = 0;            // your network key index number (needed only for WEP)
WiFiClient  client;

void setup() {
  Serial.begin(115200);
  delay(100);

  WiFi.mode(WIFI_STA);

}

void loop() {

  // Connect or reconnect to WiFi
  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    while (WiFi.status() != WL_CONNECTED) {
      WiFi.begin(ssid, pass); // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);
    }
    Serial.println("\nConnected.");
  }

  // Measure Signal Strength (RSSI) of Wi-Fi connection
  long rssi = WiFi.RSSI();

  Serial.println("RSSI"+String(rssi));

  // Wait 20 seconds to uodate the channel again
//  delay(20000);
  delay(1000);
}
//#include <Arduino.h>
//#include <ESP8266WiFi.h>
//
//const char* SSID = "YOUR_SSID";
//
//// Return RSSI or 0 if target SSID not found
//int32_t getRSSI(const char* target_ssid) {
//  byte available_networks = WiFi.scanNetworks();
//
//  for (int network = 0; network < available_networks; network++) {
//    if (strcmp(WiFi.SSID(network), target_ssid) == 0) {
//      return WiFi.RSSI(network);
//    }
//  }
//  return 0;
//}
//
//void setup() {
//  Serial.begin(115200);
//}
//
//void loop() {
//  unsigned long before = millis();
//  int32_t rssi = getRSSI(SSID);
//  unsigned long after = millis();
//  Serial.print("Signal strength: ");
//  Serial.print(rssi);
//  Serial.println("dBm");
//
//  Serial.print("Took ");
//  Serial.print(after - before);
//  Serial.println("ms");
//  delay(1000);
//}
