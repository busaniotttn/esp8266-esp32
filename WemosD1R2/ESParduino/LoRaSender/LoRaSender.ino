#include <SPI.h>
#include <LoRa.h>
#include "dragino_definitions.h"
int counter = 0;

void setup() {
  Serial.begin(9600);
  while (!Serial);

  Serial.println("LoRa Sender");
//  SPI.begin(SCK,MISO,MOSI,SS); //ESP32 no ESP8266
  LoRa.setPins(SS,RST,DI0);  
  if (!LoRa.begin(915E6)) {
//  if(!Lora.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

void loop() {
  Serial.print("Sending packet: ");
  Serial.println(counter);

  // send packet
  LoRa.beginPacket();
  LoRa.print("hello ");
  LoRa.print(counter);
  LoRa.endPacket();

  counter++;

  delay(5000);
}
