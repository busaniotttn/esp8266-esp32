// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 868.0
//Lora.h
// Dragino_LoRa_v1.2 ports
// GPIO13  -- SX1278's SCK
// GPIO11 -- SX1278's MISO
// GPIO12 -- SX1278's MOSI
// GPIO10 -- SX1278's CS
// GPIO9 -- SX1278's RESET
// GPIO2 -- SX1278's IRQ(Interrupt Request)

#define SS      10
#define RST     9
#define DI0     2
