#include <SPI.h>
#include <LoRa.h>
#include <ESP8266WiFi.h>
#include "wemos_definitions.h"
int sta=-1;
void setup() {

  Serial.begin(115200);
  Serial.print("Setting soft-AP ... ");
  boolean result = WiFi.softAP("raspi-webgui", "ChangeMe");
  if(result == true)
  {
    Serial.println("Ready");
  }
  else
  {
    Serial.println("Failed!");
  }

  while (!Serial);

  Serial.println("LoRa Receiver");
//  SPI.begin(SCK,MISO,MOSI,SS); //ESP32 no ESP8266
  LoRa.setPins(SS,RST,DI0);  
  if (!LoRa.begin(915E6)) {
//  if(!Lora.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

void loop() {
  if (WiFi.softAPgetStationNum()!=sta){
  Serial.printf("Stations connected = %d\n", WiFi.softAPgetStationNum());
//  delay(3000);
  sta=WiFi.softAPgetStationNum();
  }
  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    Serial.print("Received packet '");

    // read packet
    while (LoRa.available()) {
      Serial.print((char)LoRa.read());
    }

    // print RSSI of packet
    Serial.print("' with RSSI ");
    Serial.println(LoRa.packetRssi());
  }
}
