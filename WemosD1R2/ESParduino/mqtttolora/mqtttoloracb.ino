/*
 * uMQTTBroker demo for Arduino
 * 
 * The program starts a broker, subscribes to anything and publishs a topic every second.
 * Try to connect from a remote client and publish something - the console will show this as well.
 */
#include <SPI.h>
#include <LoRa.h>
#include <Wire.h>  
#include <ESP8266WiFi.h>
#include "uMQTTBroker.h"
// Pin definetion LoRa
#include "wemos_definitions.h"
int counter = 0;
/*
 * Your WiFi config here
 */
char ssid[] = "uMQTT2LoRa";    // your network SSID (name)
char pass[] = "ChangeMe"; // your network password


unsigned int mqttPort = 1883;       // the standard MQTT broker port
unsigned int max_subscriptions = 30;
unsigned int max_retained_topics = 30;
void sendMessage(String outgoing){
      while (LoRa.beginPacket() == 0) {
        Serial.print("waiting for radio ... ");
        delay(100);
      }
      Serial.print("Sending packet non-blocking: ");
      Serial.println(counter);
      // send packet
      LoRa.beginPacket();
      LoRa.print(outgoing);
//      LoRa.print(counter);
      LoRa.endPacket(true);
      Serial.println(outgoing);
//      MQTT_server_cleanupClientCons();
      counter++;
}
void data_callback(uint32_t *client /* we can ignore this */, const char* topic, uint32_t topic_len, const char *data, uint32_t lengh) {
  char topic_str[topic_len+1];
  os_memcpy(topic_str, topic, topic_len);
  topic_str[topic_len] = '\0';
  char data_str[lengh+1];
  os_memcpy(data_str, data, lengh);
  data_str[lengh] = '\0';

//  Serial.print("received topic '");
//  Serial.print(topic_str);
//  Serial.print("' with data '");
//  Serial.print(data_str);
//  Serial.println("'");
  sendMessage(data_str);
}
void initlora(){
//  SPI.begin(SCK,MISO,MOSI,SS);
  LoRa.setPins(SS,RST,DI0);
  
//  while (!LoRa.begin(BAND,PABOOST))
Serial.println("Initialising LoRa module....");
  while (!LoRa.begin(BAND))
  {
    Serial.print(".");
  }
  
  Serial.println("LoRa Init success!");
  delay(1000);
  
//  LoRa.onReceive(onReceive);
  
 }
void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println();

//  // We start by connecting to a WiFi network
//  Serial.print("Connecting to ");
//  Serial.println(ssid);
//  WiFi.begin(ssid, pass);
//  
//  while (WiFi.status() != WL_CONNECTED) {
//    delay(500);
//    Serial.print(".");
//  }
//  Serial.println("");
//  
//  Serial.println("WiFi connected");
//  Serial.println("IP address: ");
//  Serial.println(WiFi.localIP());
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid, pass);
  Serial.println("AP started");
  Serial.println("IP address: " + WiFi.softAPIP().toString());
/*
 * Register the callback
 */
  MQTT_server_onData(data_callback);
  initlora();
/*
 * Start the broker
 */
  Serial.println("Starting MQTT broker");
  MQTT_server_start(mqttPort, max_subscriptions, max_retained_topics);

/*
 * Subscribe to anything
 */
  MQTT_local_subscribe((unsigned char *)"#", 0);
}

void loop()
{
  // String myData(counter++);

/*
 * Publish the counter value as String
 */
  // MQTT_local_publish((unsigned char *)"/MyBroker/count", (unsigned char *)myData.c_str(), myData.length(), 0, 0);
//  sendMessage("data_str");
  // wait a second
  delay(1000);
}
