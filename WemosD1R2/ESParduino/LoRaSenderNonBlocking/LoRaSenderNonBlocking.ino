#include <SPI.h>
#include <LoRa.h>
#include "wemos_definitions.h"
int counter = 0;

void setup() {
  Serial.begin(9600);
  while (!Serial);

  Serial.println("LoRa Sender non-blocking");
  // override the default CS, reset, and IRQ pins (optional)
//  SPI.begin(SCK,MISO,MOSI,SS); //ESP32 no ESP8266
  LoRa.setPins(SS, RST, DI0);// set CS, reset, IRQ pin
  if (!LoRa.begin(915E6)) {
//  if(!Lora.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

void loop() {
  // wait until the radio is ready to send a packet
  while (LoRa.beginPacket() == 0) {
    Serial.print("waiting for radio ... ");
    delay(100);
  }

  Serial.print("Sending packet non-blocking: ");
  Serial.println(counter);

  // send in async / non-blocking mode
  LoRa.beginPacket();
  LoRa.print("hello ");
  LoRa.print(counter);
  LoRa.endPacket(true); // true = async / non-blocking mode

  counter++;
}
