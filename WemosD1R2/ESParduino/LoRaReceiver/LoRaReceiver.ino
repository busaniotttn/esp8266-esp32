#include <SPI.h>
#include <LoRa.h>
#include "wemos_definitions.h"
void setup() {
  Serial.begin(115200);
  while (!Serial);

  Serial.println("LoRa Receiver");
//  SPI.begin(SCK,MISO,MOSI,SS); //ESP32 no ESP8266
  LoRa.setPins(SS,RST,DI0);  
  if (!LoRa.begin(915E6)) {
//  if(!Lora.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }
}

void loop() {
  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    Serial.print("Received packet '");

    // read packet
    while (LoRa.available()) {
      Serial.print((char)LoRa.read());
    }

    // print RSSI of packet
    Serial.print("' with RSSI ");
    Serial.println(LoRa.packetRssi());
  }
}
